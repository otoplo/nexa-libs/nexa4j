package com.otoplo.nexa4j.rpc.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.otoplo.nexa4j.rpc.model.transaction.TransactionInfo;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonFormat(with = JsonFormat.Feature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
public class Block {

    private String hash;
    private long confirmations;
    private long height;
    private int size;
    private int txCount;
    private String merkleRoot;
    private long time;
    private long medianTime;
    private String nonce;
    private String bits;
    private BigDecimal difficulty;
    private String chainWork;
    private String utxoCommitment;
    private String minerData;
    private String status;
    private boolean onMainChain;
    private String previousBlockHash;
    private String ancestorHash;
    private String nextBlockHash;

    @JsonProperty("tx")
    private List<TransactionInfo> transactions;
}
