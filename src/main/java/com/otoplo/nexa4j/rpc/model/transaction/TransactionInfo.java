package com.otoplo.nexa4j.rpc.model.transaction;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonFormat(with = JsonFormat.Feature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
public class TransactionInfo {

    private String txId;
    private String txIdem;
    private int version;
    private int size;
    private long lockTime;
    private BigDecimal spends;
    private BigDecimal sends;
    private BigDecimal fee;
    private String hex;

    @JsonProperty("vin")
    private List<TxIn> inputs;

    @JsonProperty("vout")
    private List<TxOut> outputs;
}
