package com.otoplo.nexa4j.rpc.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@Setter
@ToString
@JsonFormat(with = JsonFormat.Feature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
public class WalletInfo {

    private double walletVersion;

    private String syncBlock;

    private long syncHeight;

    private BigDecimal balance;

    @JsonProperty("unconfirmed_balance")
    private BigDecimal unconfirmedBalance;

    @JsonProperty("immature_balance")
    private BigDecimal immatureBalance;

    private long txCount;

    private long keyPoolOldest;

    private long keyPoolSize;

    private BigDecimal payTxFee;

    private String hdMasterKeyId;
}
