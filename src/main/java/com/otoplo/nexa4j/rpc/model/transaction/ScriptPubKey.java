package com.otoplo.nexa4j.rpc.model.transaction;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class ScriptPubKey {

    private String asm;
    private String hex;
    private String type;
    private String scriptHash;
    private String argsHash;
    private String group;
    private BigDecimal groupQuantity;
    private String groupAuthority;
    private List<String> addresses;
}
