package com.otoplo.nexa4j.rpc.model.transaction;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class TxOut {

    private int type;
    private String outpoint;
    private BigDecimal value;
    private ScriptPubKey scriptPubKey;

    @JsonProperty("n")
    private int index;
}
