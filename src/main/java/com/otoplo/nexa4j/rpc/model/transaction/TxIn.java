package com.otoplo.nexa4j.rpc.model.transaction;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class TxIn {

    private String outpoint;
    private BigDecimal amount;
    private ScriptSig scriptSig;
    private long sequence;
}
